﻿using board2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBatisNet.DataMapper;

namespace board2.DAO.mapper
{
    public class BoardService
    {

        public IList<Board> findBoardList(Board board)
        {
             return Mapper.Instance().QueryForList<Board>("findBoardList", board);
        }

        public int findBoardCount(Board board)
        {
            return Mapper.Instance().QueryForObject<int>("findBoardCount", board);
        }

        public Board findBoardItem(Board board)
        {
            return Mapper.Instance().QueryForObject<Board>("findBoardItem", board);
        }

        public void insertBoard(Board board)
        {
            Mapper.Instance().Insert("insertBoard", board);
        }

        public void updateBoard(Board board)
        {
            Mapper.Instance().Update("modifyBoard", board);
        }

        public void deleteBoard(Board board)
        {
            Mapper.Instance().Delete("deleteBoard", board);
        }

    }
}
