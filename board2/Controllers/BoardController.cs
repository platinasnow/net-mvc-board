﻿using board2.DAO.mapper;
using board2.Models;
using board2.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace board2.Controllers
{
    public class BoardController : Controller
    {
        BoardService boardService = new BoardService();

        public ActionResult list(Board board, Pagination pagination)
        {
            //System.Diagnostics.Debug.Write(page + "\n");
            pagination.setTotalItemCount(boardService.findBoardCount(board));
            ViewBag.list = boardService.findBoardList(board);
            ViewBag.pagination = pagination;
            ViewBag.board = board;
            return View();

        }

        public ActionResult register()
        {
            return View();
        }

        public ActionResult registerSubmit(Board board)
        {
            boardService.insertBoard(board);   
            return RedirectToAction("list", "Board");
        }

        public ActionResult modify(Board board)
        {
            ViewBag.item = boardService.findBoardItem(board);
            ViewBag.board = board;
            return View();
        }

        public ActionResult modifySubmit(Board board)
        {
            boardService.updateBoard(board);
            return RedirectToAction("list", "Board");
        }

        public ActionResult delete(Board board)
        {
            boardService.deleteBoard(board);
            return RedirectToAction("list", "Board");
        }

        public ActionResult view(Board board)
        {
            ViewBag.item = boardService.findBoardItem(board);
            ViewBag.board = board;
            return View();
        }


    }
}
