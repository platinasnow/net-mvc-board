﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace board2.Util
{
    public class SearchInfo : Pagination
    {
        public String searchInput { get; set; }
        public String searchValue { get; set; }

    }
}