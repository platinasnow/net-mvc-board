﻿using board2.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace board2.Models
{
    [Bind]
    public class Board : SearchInfo
    {
        public int idx { get; set; }
        public string title { get; set; }
        public string contents { get; set; }
        public string name { get; set; }
        public string regId { get; set; }
        public DateTime regDate { get; set; }

    }
}
